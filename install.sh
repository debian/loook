#!/bin/bash
# -*- coding: utf-8 -*-
#
#  install-loook.sh
#
#  Copyright 2013,2014 Mechtilde Stehmann
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

InstP="/usr/local/lib/loook"
ManP="/usr/share/man"

if test $(whoami) == "root"

then

        if test -e "/usr/bin/python3"

        then

        mkdir -pv $InstP &&

        cp -av loook.py $InstP/loook.py &&
        chmod +x $InstP/loook.py &&

        mkdir -pv $InstP/locale/en/LC_MESSAGES/ &&
        cp -av locale/en/LC_MESSAGES/loook.mo \
        $InstP/locale/en/LC_MESSAGES/loook.mo &&

        mkdir -pv $InstP/locale/de/LC_MESSAGES/ &&
        cp -av locale/de/LC_MESSAGES/loook.mo \
        $InstP/locale/de/LC_MESSAGES/loook.mo &&

        mkdir -pv $InstP/locale/nl/LC_MESSAGES/ &&
        cp -av locale/nl/LC_MESSAGES/loook.mo \
        $InstP/locale/nl/LC_MESSAGES/loook.mo &&

        mkdir -pv $InstP/locale/it/LC_MESSAGES/ &&
        cp -av locale/it/LC_MESSAGES/loook.mo \
        $InstP/locale/it/LC_MESSAGES/loook.mo &&

        mkdir -pv $InstP/locale/cs/LC_MESSAGES/ &&
        cp -av locale/cs/LC_MESSAGES/loook.mo \
        $InstP/locale/cs/LC_MESSAGES/loook.mo &&

        mkdir -pv $InstP/locale/es/LC_MESSAGES/ &&
        cp -av locale/es/LC_MESSAGES/loook.mo \
        $InstP/locale/es/LC_MESSAGES/loook.mo &&

        mkdir -pv $InstP/locale/fr/LC_MESSAGES/ &&
        cp -av locale/fr/LC_MESSAGES/loook.mo \
        $InstP/locale/fr/LC_MESSAGES/loook.mo &&

        ln -sv $InstP/loook.py /usr/local/bin/loook &&

        mkdir -pv $ManP/man1/ &&
        gzip man/loook.1 > $ManP/man1/loook.1.gz &&

        mkdir -pv $ManP/de/man1/ &&
        gzip man/de/loook.1 > $ManP/de/man1/loook.1.gz &&

        cp -av loook.desktop /usr/share/applications/loook.desktop &&
        cp -av loook.png /usr/share/pixmaps/loook.png

        echo "You can start loook from the menu."

        else
                echo "Python3 isn't installed.\nPlease install first!"
        fi

else
    echo "You must be root!"
fi

exit
